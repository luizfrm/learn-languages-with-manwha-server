const express = require("express");
const bodyParser = require("body-parser");
const axios = require("axios");
const { parse } = require("node-html-parser");
const cors = require("cors");
const settings = require("./settings.json");

const app = express();
app.use(cors());
app.use(bodyParser.json());

const port = process.env.PORT || 4300;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

let imgUrls = [];

app.get("/getImagesFromUrl", async (req, res) => {
  const { url } = req.query;

  try {
    const response = await axios.get(url);
    const htmlContent = response.data;

    imgUrls = parseImageUrls(htmlContent, url);

    if (imgUrls.length > 0) {
      return res.status(200).json({ imageUrls: imgUrls });
    } else {
      return res
        .status(404)
        .json({ error: "No images found in the provided URL" });
    }
  } catch (error) {
    console.error("Error fetching images:", error);
    return res.status(500).json({ error: "Error fetching the provided URL" });
  }
});

const parseImageUrls = (htmlContent, url) => {
  const protocolAndDomain = url.match(/^https?:\/\/[^/]+/);

  const root = parse(htmlContent);

  const imageElements = root.querySelectorAll("img");
  const imageUrls = [];

  imageElements.forEach((img) => {
    settings.typesOfImage.forEach((type) => {
      addImg(img, type, imageUrls, protocolAndDomain);
    });
  });

  return imageUrls;
};

const SHOW_JUST_OUTER_URL_IMGS = false;
const addImg = (img, srcAttribute, imageUrls, protocolAndDomain) => {
  const src = img.getAttribute(srcAttribute);
  if (src) {
    if (SHOW_JUST_OUTER_URL_IMGS) {
      if (
        src.substring(0, protocolAndDomain[0].length) != protocolAndDomain[0]
      ) {
        imageUrls.push(src);
      }
    } else {
      imageUrls.push(src);
    }
  }
};

app.get("/proxy-image", async (req, res) => {
  const { page } = req.query;

  if (!imgUrls[page]) {
    return res.status(404).json({ error: "Image not found" });
  }

  try {
    const response = await axios.get(imgUrls[page], {
      responseType: "stream",
    });

    response.data.pipe(res);
  } catch (error) {
    console.error("Error fetching image:", error);
    res.status(500).send("Internal Server Error");
  }
});
